# PluralNotes-GUI

Multiple users, multiple notes- now with a graphical interface!

Pluralnotes is a simple notebook utility designed with multiple users in mind. It allows you to create multiple users on the same OS user, making it easy to keep several people's notes separate without ever logging off on the OS level.

As of now, Pluralnotes GUI is functional! More features are in the works.

## Current features

- Notes can be created, edited, deleted, and viewed (currently buggy and broken- working on it, sorry!).
- Users can be added and logged into. Users each have their own personal directory for personal notes in addition to the shared notes directory.
- A shared directory exists for keeping group notes or communicating with other users.
- Per-user customization of font size and family!

## Planned features

- Note searching
- Renaming, deleting, and searching users (to delete users in the meantime, go inside the notes directory and delete their folder)
- Archive directory for deleted users' notes
- Ability to move notes to shared or archive directories
- Changing users without restarting the program
- Color customization (partially implemented; settings exist but are not yet used)

## How to use

Pluralnotes is written in Python 3. It works on Linux for certain, and support for other operating systems is planned. You'll need to install Python to run this; you can find it in your distribution's repositories or <a href="https://www.python.org/downloads/">snag it directly from the Python website</a>.

To use Pluralnotes, please download a copy of this repository (either using git or by clicking the "Download Repository" button at the top right, just above the list of files). Place the repository wherever you'd like and run pluralnotes.py.

## Other

<a href="https://codeberg.org/Candlebrae/pluralnotes">The original PluralNotes is a terminal-based program and can be found here</a>.