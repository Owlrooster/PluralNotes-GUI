#!/usr/bin/env python3 

import tkinter
import tkinter.font
import os

username = ""

class Login_Window:
    def __init__(self):
        # Initialize window
        self.root = tkinter.Tk()
        self.root.title("Pluralnotes")
        self.root.geometry = "600x900"

        # Set up fonts
        self.verdana_font = tkinter.font.Font(family="Verdana", size=15)

        self.build_login_window()

        self.root.mainloop()

    def get_users(self):
        return os.listdir("./notes")

    def build_login_window(self):
        # Greeting text
        self.greeting = tkinter.Label(self.root, text="Create a new user:", 
        font=self.verdana_font)

        # Username entry area
        self.username_frame = tkinter.Frame()
        self.username_label = tkinter.Label(self.username_frame, 
            text="Your name: ", font=self.verdana_font)
        self.username_entry = tkinter.Entry(self.username_frame, width=20, font=self.verdana_font)
        # Enter key event handling
        self.username_entry.bind("<Return>", self.handle_return)
        # Pack username entry
        self.username_label.pack(side="left", padx=5)
        self.username_entry.pack(side="right")

        # Label for clarification
        self.option_label = tkinter.Label(self.root, 
            text="Or select an existing user:", font=self.verdana_font)

        # List existing users
        self.listframe = tkinter.Frame()
        self.__users = tkinter.Variable(value=self.get_users())
        self.user_listbox = tkinter.Listbox(
            self.listframe, 
            height=5, 
            width=30,
            listvariable=self.__users, 
            font=self.verdana_font
            )
        # Scrollbar for listbox
        self.scrollusers=tkinter.Scrollbar(
            self.listframe, 
            orient="vertical", 
            command=self.user_listbox.yview
            )

        self.button_frame = tkinter.Frame()
        # Login button
        self.login_button = tkinter.Button(self.button_frame, text="Login", 
            font=self.verdana_font, command=self.login)
        # Quit button
        self.quit = tkinter.Button(self.button_frame, text="Quit", 
            font=self.verdana_font, command=self.kill_window)

        # Pack everything
        self.greeting.pack(pady=10)
        self.username_frame.pack(pady=10, padx=20)
        self.option_label.pack(pady=10)
        self.user_listbox.pack(side="left")
        self.scrollusers.pack(side="right", fill="y")
        self.user_listbox.config(yscrollcommand=self.scrollusers.set)
        self.listframe.pack(padx=5, pady=5)
        self.login_button.pack(padx=5, side="left")
        self.quit.pack(padx=5, side="right")
        self.button_frame.pack(pady=10)

        # Center the window
        self.root.eval('tk::PlaceWindow . center')

    # Run when the return key is pressed in self.username_entry
    def handle_return(self, event):
        self.login()

    # Used for existing users (and new users after creation)
    def login(self):
        self.__username = self.username_entry.get()
        if self.__username != "":
            self.make_user_folder()
        else:
            try:
                self.__username = str(self.user_listbox.get(self.user_listbox.curselection()))
            except:
                self.__username = "sharednotes"
        #self.make_user_folder()
        self.cd_user_dir()
        self.kill_window()

    def get_username(self):
        return self.__username.strip()

    def make_user_folder(self):
        # If folder doesn't exist, create it
        if os.path.isdir("./notes/" + self.__username) == False:
            os.mkdir("./notes/" + self.__username)

    def cd_user_dir(self):
        userDir = "./notes/" + self.__username
        os.chdir(userDir)

    def kill_window(self):
        self.root.quit()
        self.root.destroy()