#!/usr/bin/env python3 

import tkinter
import tkinter.font
from tkinter import scrolledtext
import tkinter.messagebox
import os

class Note_Editor:
    def __init__(self, user, content, filename, fontsize):
        self.__user = user
        self.__content = content

        # Initialize window
        self.root = tkinter.Tk()
        self.root.title("Note Editor")

        # Set up fonts
        self.verdana_font = tkinter.font.Font(family="Verdana", size=fontsize)

        # Save location prompt
        self.save_frame = tkinter.Frame(self.root)
        self.save_prompt = tkinter.Label(self.save_frame, text="Note name:", font=self.verdana_font)
        self.save_entry = tkinter.Entry(self.save_frame, width=50, font=self.verdana_font)
        self.save_entry.insert(tkinter.INSERT, filename)
        # Pack prompt
        self.save_prompt.pack(side="left", padx=10)
        self.save_entry.pack(side="left", padx=10)

        # Buttons
        self.save_button = tkinter.Button(self.save_frame, text="Save", 
            font=self.verdana_font, command=self.check_and_save_file)
        self.close_button = tkinter.Button(self.save_frame, text="Close", 
            font=self.verdana_font, command=self.kill_it_all)
        self.save_button.pack(side="left", padx=10)
        self.close_button.pack(side="left", padx=5)

        # Text entry
        self.note_entry = scrolledtext.ScrolledText(self.root, 
            wrap=tkinter.WORD, font=self.verdana_font)
        # Insert any notes being opened (self.__content is empty string if new note)
        self.note_entry.insert(tkinter.INSERT, self.__content)

        # Pack it all
        self.note_entry.pack()
        self.save_frame.pack(pady=10)

        # Center the window and update
        self.root.eval('tk::PlaceWindow . center')
        self.root.update()
        self.root.mainloop()

    def check_and_save_file(self):
        # Check if the file already exists. Prompt user to overwrite or append
        # if it does exist; else, just save it to a new file with that name
        # If the frame isn't created here, an error is thrown later when checking if it's packed
        self.check_frame = tkinter.Frame(self.root)
        if self.save_entry.get().strip() != "":
            self.save_entry = self.save_entry.get()
            if self.save_entry == "settings.ini":
                self.__savefile_name = "settings.ini"
            else:
                self.__savefile_name = self.save_entry.get() + ".txt"
            if os.path.isfile(self.__savefile_name):
                # Add buttons and ask the user what to do
                self.check_label = tkinter.Label(self.check_frame, 
                    text="This note already exists! Overwrite, or append?", font=self.verdana_font)
                self.overwrite_button = tkinter.Button(self.check_frame, text="Overwrite",
                    font=self.verdana_font, command=lambda: self.save_file("w"))
                self.append_button = tkinter.Button(self.check_frame, text="Append",
                    font=self.verdana_font, command=lambda: self.save_file("a"))
                self.cancel_button = tkinter.Button(self.check_frame, text="Cancel", 
                    font=self.verdana_font, command=self.kill_it_all)
                # Pack it
                self.check_label.pack(side="left", padx=10)
                self.overwrite_button.pack(side="left", padx=5)
                self.append_button.pack(side="left", padx=5)
                self.cancel_button.pack(side="left", padx=5)
                self.check_frame.pack(pady=10)
                self.root.update()
            else:
                self.save_file("w")
        else:
            # The user screwed up and forgot a filename
            tkinter.messagebox.showinfo("Pluralnotes", "Please enter a filename.", parent=self.root)

    # Expects save mode (w, a, etc) as a string
    def save_file(self, mode):
        self.__savefile = open(self.__savefile_name, mode)
        self.__savefile.write(self.note_entry.get("1.0", tkinter.END))
        self.__savefile.close()
        if self.check_frame.winfo_exists() == True: self.check_frame.destroy()

    def get_savefile_name(self):
        return self.savefile_name

    def kill_it_all(self):
        self.root.quit()
        self.root.destroy()