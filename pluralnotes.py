#!/usr/bin/env python3 

import tkinter
import tkinter.font
import modules.login as login
import modules.note_editor as notepad
import os
import configparser

class PluralNotesMain:
    def __init__(self, username):
        self.__user = username
        self.create_config()
        self.read_config()
        self.build_window()

    def create_config(self):
        if os.path.isfile("settings.ini") == False:
            config = configparser.ConfigParser()
            config['font'] = {'font_size' : '18', 'font_family' : 'Arial'}
            config['color'] = {'font_color' : 'default', 'background_color' : 'default'}
            with open('settings.ini', 'w') as configfile:
                config.write(configfile)

    def read_config(self):
        config = configparser.ConfigParser()
        config.read('settings.ini')
        self.font_size = config['font']['font_size']
        try: int(self.font_size)
        except: 
            self.font_size = 18
            config['font'] = {'font_size' : '18', 'font_family' : 'Verdana'}
            with open('settings.ini', 'w') as configfile:
                config.write(configfile)
        self.font_family = config['font']['font_family']
        self.background_color = config['color']['background_color']
        self.font_color = config['color']['font_color']


    def open_notepad(self, content, filename):
        notepad.Note_Editor(self.__user, content, filename, self.font_size)
        self.update_listbox()

    def get_notes(self):
        self.__notelist = os.listdir()
        self.__notelist = sorted(self.__notelist, key=str.lower)
        return self.__notelist

    def get_selection(self):
        return str(self.note_listbox.get(self.note_listbox.curselection()))

    def open_note(self):
        self.selection = self.get_selection()
        self.selection_file = open(self.selection, "r")
        self.open_notepad(self.selection_file.read(), self.selection.rstrip(".txt"))
        self.selection_file.close()
        self.update_listbox()

    def open_settings(self):
        self.selection_file = open("settings.ini", "r")
        self.open_notepad(self.selection_file.read(), "settings.ini")
        self.selection_file.close()
        self.update_listbox()

    def delete_note(self):
        self.selection = self.get_selection()
        os.remove(self.selection)
        self.update_listbox()

    def update_listbox(self):
        self.__notes.set(self.get_notes())

    def build_window(self):
        # Initialize the main window
        self.root = tkinter.Tk()
        self.root.title("Pluralnotes")
        self.verdana_font = tkinter.font.Font(family=self.font_family, size=self.font_size)

        # Greeting text frame
        self.hello_frame = tkinter.Frame()
        self.greeting = tkinter.Label(
            self.hello_frame, 
            text="Welcome to your notes, ", 
            font=self.verdana_font
            )
        # Display username as feedback that login worked
        self.username = tkinter.StringVar()
        self.username_label = tkinter.Label(
            self.hello_frame, 
            textvariable=self.username, 
            font=self.verdana_font
            )
        self.username.set(self.__user + "!")

        # List existing notes
        self.listframe = tkinter.Frame()
        self.__notes = tkinter.Variable(value=self.get_notes())
        self.note_listbox = tkinter.Listbox(
            self.listframe, 
            height=10, 
            width=60,
            listvariable=self.__notes, 
            font=self.verdana_font
            )
        # Scrollbar for listbox
        self.scrollnotes=tkinter.Scrollbar(
            self.listframe, 
            orient="vertical", 
            command=self.note_listbox.yview
            )

        # Offer options as buttons
        self.button_frame = tkinter.Frame()
        self.create_note = tkinter.Button(
            self.button_frame, 
            text="New", 
            font=self.verdana_font, 
            command=lambda: self.open_notepad("", ""))
        self.read_notes = tkinter.Button(
            self.button_frame, 
            text="Open", 
            font=self.verdana_font, 
            command=self.open_note)
        self.delete_notes = tkinter.Button(
            self.button_frame, 
            text="Delete", 
            font=self.verdana_font, 
            command=self.delete_note)
        self.settings = tkinter.Button(
            self.button_frame, 
            text="Settings", 
            font=self.verdana_font,
            command=self.open_settings)
        self.exit_frame = tkinter.Frame()
        self.quit = tkinter.Button(
            self.exit_frame, 
            text="Quit", 
            command=self.root.destroy, font=self.verdana_font)

        # Pack everything
        self.greeting.pack(side="left")
        self.username_label.pack(side="left")
        self.hello_frame.pack(padx=20, pady=20)

        self.note_listbox.pack(side="left")
        self.scrollnotes.pack(side="right", fill="y")
        self.note_listbox.config(yscrollcommand=self.scrollnotes.set)
        self.listframe.pack(padx=20, pady=20)

        self.create_note.pack(padx=5, side="left")
        self.read_notes.pack(padx=5, side="left")
        self.delete_notes.pack(padx=5, side="left")
        self.settings.pack(padx=5, side="left")
        self.button_frame.pack(pady=5, padx=10, side="left")

        self.quit.pack(padx=5, side="left")
        self.exit_frame.pack(pady=20, padx=10, side="right")

        # Center the window and loop
        self.root.eval('tk::PlaceWindow . center')
        self.root.mainloop()


def main():
    login_window = login.Login_Window()
    username = login_window.get_username()
    main_window = PluralNotesMain(username)

main()